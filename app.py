# -*- coding: utf-8 -*-
"""Flask concepts testing app module.

Compatible with Python2/3 (as much as possible).

Defines a basic index listing available endpoints (templates/index.html).

"""


import flask
import apicall


app = flask.Flask(__name__)  # THE app


@app.route('/')
def index():
    """Application index."""
    return flask.render_template('index.html')


#  Registers API call blueprint
app.register_blueprint(apicall.api)
