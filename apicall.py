# -*- coding: utf-8 -*-
"""API Call from Jinja concepts blueprint module."""


import uuid
import flask


api = flask.Blueprint('api', __name__)
""":obj:`flask.Blueprint`: API call blueprint instance."""


def my_api_call(log_id):
    """Fake API call.

    Args:
        log_id (str): An individual ID string.

    Returns:
        str: The hexadecimal string of the UTF-8 bytes of the ID string.

    """
    return bytes(log_id, encoding='UTF-8').hex()


class CustomerLog(object):
    """Customer Log ID wrapper object.
    
    Attrs:
        log_id (str): Wrapper customer log ID string.

    """

    def __init__(self, log_id):
        """Constructor.

        Args:
            log_id (str): Customer log ID string.

        """
        self.log_id = log_id

    def __str__(self):
        """String representation of the object.

        Useful for print statements or {{ instance }} in Jinja templates.

        """
        return str(self.log_id)

    def my_api_call(self):
        """Fake API call wrapper.

        Calls Fake API call on wrapped ID.

        """
        return my_api_call(self.log_id)


@api.context_processor  # Flask decorator
def add_api_call():
    """Add API call to Jinja context."""
    #  Exposes the `my_api_call` function as ``my_api_call`` in Jinja templates
    return {'my_api_call': my_api_call}


def get_ids():
    """Generate fake IDs.

    Returns:
        :obj:`list` of :obj:`str`: List of fake customer log ID strings.

    """
    #  Generates a list of 10 random UUID strings
    return [str(uuid.uuid4()) for _ in range(10)]


@api.route('/api')
def index():
    id_list = get_ids()
    return flask.render_template(
        'api/index.html',
        ids_list=[CustomerLog(_id) for _id in id_list]
    )
